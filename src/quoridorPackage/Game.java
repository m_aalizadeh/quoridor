package quoridorPackage;


import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public abstract class Game implements Comparable<Game>{
    
    Board board;
    Player[] players;
//    GameValidator gv;
    MainUI mainUI;

    /**
     * Create a new game with an array of players 
     * and a 9*9 board of boardCells
     * @param players
     */
    
    public Game(Player[] players) {
        this.players = players; 
        this.board = new Board(players);
//        gv = new GameValidator(board);
//        mainUI = new MainUI();
//        mainUI.initBoard();
    }
    
    
    /**
     * Get a copy of the current game board.
     * @return 
     * @throws java.lang.CloneNotSupportedException 
     */
    public Board getBoard() throws CloneNotSupportedException {
        return board.clone();
    }
    
    /**
     * Checks each players position 
     * and returns true if any player won, false if not.
     * @return 
     */
    public boolean finished() {
        boolean finished = false;
        for (Piece current : board.pieces) {
            Position position = board.positionOf(current);
            if (current.getEnd() == Direction.UP) {
                finished = finished || position.getRow() == 0;
            } else if (current.getEnd() == Direction.DOWN) {
                finished = finished || position.getRow() == 8;
            } else if (current.getEnd() == Direction.LEFT) {
                finished = finished || position.getCol() == 0;
            } else if (current.getEnd() == Direction.RIGHT) {
                finished = finished || position.getCol() == 8;
            }
        }
        return finished;
    }
    
    /**
     * Returns winner of the game (or null if not yet set)
     * @return 
     */
    public Piece getWinner() {
        for (Piece current : board.pieces) {
            Position position = board.positionOf(current);
            if (current.getEnd() == Direction.UP) {
                if (position.getRow() == 0) { return current; }
            } else if (current.getEnd() == Direction.DOWN) {
                if (position.getRow() == 8) { return current; }
            } else if (current.getEnd() == Direction.LEFT) {
                if (position.getCol() == 0) { return current; }
            } else if (current.getEnd() == Direction.RIGHT) {
                if (position.getCol() == 8) { return current; }
            }
        }
        return null;
    }
    
    /**
     * Play a game until finished.
     * @throws java.lang.CloneNotSupportedException
     */
    public void play() throws CloneNotSupportedException {
        while (!finished()) {
            String next = players[board.getCurrentPieceNum() - 1].getAction(this);
            if (Game.this.isValidAction(next)) {
                    board = applyAction(next);
            }
        }
    }
    
        
    /**
     * Create a copy of the board with the relocatePiece applied. 
     * @param action
     * @return the resulting board (or null)
     * @throws java.lang.CloneNotSupportedException
     */
    
    public Board applyAction(String action) throws CloneNotSupportedException{
        Board retval = board.clone();
        if(action.length() == 2)
            retval = Move(new Piece(new Position(action)));
        else if(action.length() == 3){
            String orient = String.valueOf(action.charAt(2)).toUpperCase();
            retval = Block(new Wall(new Position(action), Orientation.valueOf(orient)));
        }
        if(action.length() == 3)
            retval.decRemainingWalls(retval.getCurrentPieceNum());
        retval.setCurrentPiece((retval.getCurrent() + 1) % players.length);
        return retval;
    }
    
    public Board Move(Piece piece) throws CloneNotSupportedException{
        Board retval = board.clone();
        retval.MovePiece(piece);
        return retval;
    }
    
    public Board Block(Wall wall) throws CloneNotSupportedException{
        Board retval = board.clone();
        retval.BlockWithWall(wall);
        return retval;
    }
    
    
    /**
     * Implement the special rules of player adjacency in quoridor.
     * 
     * @param p starting position
     * @param d direction to jump
     * @return list of possible moves
     */
    private Position[] jump(Position p, Direction d, boolean giveUp) {
        if (!wallExists(p, d) && playerAt(p.adjacentPos(d)) == null) {
            Position[] rval = new Position[1];
            rval[0] = p.adjacentPos(d);
            return rval;
        } else if (giveUp) {
            Position[] rval = new Position[0];
            return rval;
        } else {
            LinkedList<Position> moves = new LinkedList<>();
            for (Direction dir : Direction.values()) {
                if (!dir.equals(d) /*&& !dir.equals(d.reverse())*/) {
                    for (Position pos : jump(p, dir, true)) {
                        moves.add(pos);
                    }
                }
            }
            Position rval[] = new Position[moves.size()];
            return moves.toArray(rval);
        }
    }
    
    public boolean wallExists(Position p, Direction d) {
        return board.getBoardCells()[p.getRow()][p.getCol()].getNeighbour(d) == null;
    }
    
      /**
     * Get the id of the player at the specified position
     * @param p
     * @return 
     */
    public Integer playerAt(Position p) {
        Piece pl = board.getBoardCells()[p.getRow()][p.getCol()].getContainedPiece();
        if (pl != null) {
            return pl.getPieceNum();
        } else {
            return null;
        }
    }
    
    /**
     * Flood the board
     * @param board
     * @return if the board is floodable.
     */
        public boolean flood(Board board) {
        LinkedList<Cell> visited = new LinkedList<>();
        Queue<Cell> q = new LinkedList<>();
        q.add(board.getBoardCells()[0][0]);
        visited.add(board.getBoardCells()[0][0]);
        Cell current;
        Cell temp;
        while (!q.isEmpty()) {
            current = q.remove();
            for (Direction dir : Direction.values()) {
                temp = current.getNeighbour(dir);
                if (temp != null && !visited.contains(temp)) {
                    visited.add(temp);
                    q.add(temp);
                }
            }
        }
        return visited.size() == board.dim * board.dim;
    }
        
   
        
/////////////////////////////////////////////////////////////////////////
        
      /**
     * @param action
     * @return whether the action is valid
     */
    public boolean isValidAction(String action) throws CloneNotSupportedException {
        if (isValidActionString(action)) 
        {
            if(action.length() == 2)
                return isValidMoveAction(new Cell(new Position(action)));
            else if(action.length() == 3){
                
                String orient = String.valueOf(action.charAt(2)).toUpperCase();
                //System.out.println(orient);
                return isValidBlockAction(new Wall(new Position(action), Orientation.valueOf(orient)));
            }
        }
        return false;
    }
    
    public boolean isValidMoveAction(Cell c)
    {
        boolean validity = true;
        List<Position> adjacent = validMoves(board.positionOf(board.pieces[board.getCurrent()]));
        validity = validity && adjacent.contains(c.p);
        return validity;
    }
    public boolean isValidBlockAction(Wall wall) throws CloneNotSupportedException{
        boolean validity = true;
        if (board.getNumOfPiecesWall()[board.getCurrent()] >= 0) {
            Cell northwest = board.getBoardCells()[wall.p.getRow()][wall.p.getCol()];
            Cell northeast = board.getBoardCells()[wall.p.getRow()][wall.p.getCol() + 1];
            Cell southwest = board.getBoardCells()[wall.p.getRow() + 1][wall.p.getCol()];
            //System.out.println(wall.getOrientation());
            if (wall.getOrientation() == Orientation.V) {
                validity = validity
                        && northwest.getNeighbour(Direction.RIGHT) != null;
                validity = validity
                        && southwest.getNeighbour(Direction.RIGHT) != null;
                validity = validity
                        && !(northwest.getNeighbour(Direction.DOWN) == null && northeast
                                .getNeighbour(Direction.DOWN) == null);
                
            } else if (wall.getOrientation() == Orientation.H) {
                validity = validity
                        && northwest.getNeighbour(Direction.DOWN) != null;
                validity = validity
                        && northeast.getNeighbour(Direction.DOWN) != null;
                validity = validity
                        && !(northwest.getNeighbour(Direction.RIGHT) == null && southwest
                                .getNeighbour(Direction.RIGHT) == null);
            } else {
                validity = false;
            }
            
//            if (validity) {
//            // Test if we can flood the board
//            //Board test = board.clone();
//            Board test = Block(wall);
//            if (!flood(test)) {
//                validity = false;
//            }
//        }
            
    }
        return validity;
    }
    
       /**
     * Check if a relocatePiece string is valid.
     * @param move
     * @return 
     */
    public boolean isValidActionString(String move) {
        boolean validity = (move.length() >= 2);
        validity = validity && move.charAt(0) >= 'a' && move.charAt(0) <= 'i';
        validity = validity && move.charAt(1) >= '1' && move.charAt(1) <= '9';
        if (move.length() == 3) {
            validity = validity && (move.charAt(2) == 'V' || move.charAt(2) == 'v'|| move.charAt(2) == 'H' || move.charAt(2) == 'h');
            validity = validity && move.charAt(0) <= 'h';
            validity = validity && move.charAt(1) <= '8';

        } else if (move.length() > 3) {
            return false;
        }
        return validity;
    }
    
    /**
     * Get a list of valid relocatePiece cells from a position
     * @param pm
     * @return reachable empty squares
     */
        
    public List<Position> validMoves(Position pm) {
        LinkedList<Position> adjacent = new LinkedList<>();
        LinkedList<Position> moves = new LinkedList<>();
        LinkedList<Direction> dirs = new LinkedList<>();
        Cell current = board.getBoardCells()[pm.getRow()][pm.getCol()];
        Cell neighbour;
        Direction d;
        for (Direction dir : Direction.values()) {
            neighbour = current.getNeighbour(dir);
            if (neighbour != null) {
                boolean add = adjacent.add(neighbour.getPosition());
                dirs.add(dir);
            }
        }
        
        for (Position p : adjacent) {
            d = dirs.removeFirst();
            if (playerAt(p) != null) {
                for (Position pos: jump(p, d, false)) {
                    moves.add(pos);
                }
            } else 
            {
                moves.add(p);
            }
        }
        return moves;
    }
        
/////////////////////////////////////////////////////////////////////////
        
        
        
        
        
}
