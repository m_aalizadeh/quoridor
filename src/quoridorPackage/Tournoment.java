/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoridorPackage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;
import java.util.function.BinaryOperator;
import static jdk.nashorn.internal.objects.NativeMath.max;

/**
 *
 * @author fm
 */
public class Tournoment extends Game{
    
   private int COUNT;
   TreeSet <Game> games = new TreeSet<>();
   private int[] winners;
   private ArrayList<Integer> winner;
   Player[] players;

    public Tournoment(Player[] players) throws CloneNotSupportedException {
        super(players);
        this.players = players;
        
        if (players.length == 2)
            COUNT =3;
        else if (players.length == 4)
            COUNT = 5;
        
        winner = new ArrayList(players.length);
        for(int i=0; i<COUNT; i++){
            AddGames(players);
        }
    }

    public final void AddGames(Player[] players) throws CloneNotSupportedException {
        games.add(new Game(players) {
            @Override
            public int compareTo(Game o) {
                return 1;
            }
        });
    }

    public void PlayAllGames() throws CloneNotSupportedException {
        
        for (Game game: games){
            System.out.println(">>>>> New Round Started <<<<<");
            game.play();
            game.getBoard().printBoard();
            System.out.println("Player "+ game.getWinner().getColor() + " WON the game!");
            winner.add(game.getWinner().getPieceNum());
//            winners[game.getWinner().getPieceNum()-1]++;
        }
        
        Integer maxOccurredElement = winner.stream()
        .reduce(BinaryOperator.maxBy((o1, o2) -> Collections.frequency(winner, o1) -
                        Collections.frequency(winner, o2))).orElse(null);
        System.out.println(maxOccurredElement);
        System.out.println(">>>>> Player "+ maxOccurredElement + " Won the tournament. <<<<<");
        
    }

    @Override
    public int compareTo(Game o) {
        return 1;
    }
}
