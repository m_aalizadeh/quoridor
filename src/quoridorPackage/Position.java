
package quoridorPackage;


public class Position {
    
    
    private int row;
    private int col;
    
    //public Position parent;
    
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        char initrow = '1';
        initrow += this.row;
        char initcol = 'a';
        initcol += this.col;
        s.append(initcol);
        s.append(initrow);
        return s.toString();
    }
    
    /**
     * Create a new position from the string representation of a position.
     * @param move
     */
    public Position(String move) throws IllegalArgumentException {
        this.col = move.charAt(0) - 'a';
        this.row = move.charAt(1) - '1';
    }
    
    @Override
    public boolean equals(Object m) {
        if (!(m instanceof Position)) {
            return false;
        } else {
            Position move = (Position) m;
            boolean same = move.getRow() == getRow() && move.getCol() == getCol();
            return same;
        }
    }
    
    /**
     * Create a new position.
     * @param row
     * @param col
     */
    public Position(int row, int col) {
      
        this.row = row;
        this.col = col;
    }

    /**
     * Get the row of the position.
     * @return 
     */
    public int getRow() {
        return row;
    }
    
    /**
     * Get the column of the position.
     * @return 
     */
    public int getCol() {
        return col;
    }
    
    public void setRow(int row){
        this.row = row;
    }
    
    public void setCol(int col){
        this.col = col;
    }
    
     /**
     * Get the adjacent cell in a given direction.
     */
    public Position adjacentPos(Direction d) {
        int newrow = this.row;
        int newcol = this.col;
        
        if (d == Direction.DOWN) newrow++;
        else if (d == Direction.UP) newrow--;
        else if (d == Direction.LEFT) newcol--;
        else if (d == Direction.RIGHT) newcol++;
        
        return new Position(newrow, newcol);
    }
      
}

