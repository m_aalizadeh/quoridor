/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoridorPackage;

/**
 *
 * @author fm
 */
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MainUI extends Application {

    public static final int TILE_SIZE = 60;
    public static final int WIDTH = 9;
    public static final int HEIGHT = 9;

    private Tile[][] boardcells = new Tile[WIDTH][HEIGHT];

    private Group tileGroup = new Group();
    private Group pieceGroup = new Group();

    //////////////////////////////////
    private final int dim = 9;
    int walls = 20;
    private int current = 0;
    Piece[] pieces;
    Wall[] wallss;
    private int[] numOfPiecesWall = {10, 10};
    
    
    private Group makeBoardcells() {
        
        for (int i = 0; i < boardcells.length; i++) {
            for (int j = 0; j < boardcells.length; j++) {
                boardcells[i][j] = new Tile(new Position(i, j));
                tileGroup.getChildren().add(boardcells[i][j]);
            }
        }
        for (int i = 1; i < boardcells.length; i++) {
            for (int j = 0; j < boardcells.length; j++) {
                boardcells[i][j].setNeighbour(Direction.UP, boardcells[i - 1][j]);
            }
        }
        for (int i = 0; i < boardcells.length - 1; i++) {
            for (int j = 0; j < boardcells.length; j++) {
                boardcells[i][j].setNeighbour(Direction.DOWN, boardcells[i + 1][j]);
            }
        }
        for (int i = 0; i < boardcells.length; i++) {
            for (int j = 1; j < boardcells.length; j++) {
                boardcells[i][j].setNeighbour(Direction.LEFT, boardcells[i][j - 1]);
            }
        }
        for (int i = 0; i < boardcells.length; i++) {
            for (int j = 0; j < boardcells.length - 1; j++) {
                boardcells[i][j].setNeighbour(Direction.RIGHT, boardcells[i][j + 1]);
            }
        }
        return tileGroup;
    }
    
    
    private Group initPieces(/*Player[] players*/){
        this.pieces = new Piece[2];
        Direction[] pieces_default_dir = { Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT };
        Color[] pieces_color = {Color.BLUE, Color.RED, Color.GREEN, Color.YELLOW};
        Position[] pieces_default_pos = { new Position(8, 4), new Position(0, 4), new Position(4, 8), new Position(4, 0) };
        if (pieces.length != 2 && pieces.length != 4) { throw new IllegalArgumentException(
                "Only 2 or 4 players are supported."); }
        for (int i = 0; i < pieces.length; i++) {
            Position m = pieces_default_pos[i];
            pieces[i] = new Piece(m, i+1, pieces_color[i], pieces_default_dir[i]);
            boardcells[m.getRow()][m.getCol()].setPiece(pieces[i]);
            pieceGroup.getChildren().add(pieces[i]);
        }
        return pieceGroup;
    }
    
    public Parent initBoard(){
        
        
        Pane root = new Pane();
        root.setPrefSize(WIDTH * TILE_SIZE, HEIGHT * TILE_SIZE);
        
        //init board cells
        tileGroup = makeBoardcells();
        
        //init pieces
        pieceGroup = initPieces();
                    
        //init number of each piece's wall:
//        for(int i=0; i<numOfPiecesWall.length; i++) 
//            numOfPiecesWall[i] = walls/pieces.length;
        
        root.getChildren().addAll(tileGroup, pieceGroup);
        return root;
    }
    

//    private MoveResult tryMove(Piece piece, int newX, int newY) {
//        if (boardcells[newX][newY].hasPiece() || (newX + newY) % 2 == 0) {
//            return new MoveResult(MoveType.NONE);
//        }
//
//        int x0 = toBoard(piece.getOldX());
//        int y0 = toBoard(piece.getOldY());
//
//        if (Math.abs(newX - x0) == 1 && newY - y0 == piece.getType().moveDir) {
//            return new MoveResult(MoveType.NORMAL);
//        } else if (Math.abs(newX - x0) == 2 && newY - y0 == piece.getType().moveDir * 2) {
//
//            int x1 = x0 + (newX - x0) / 2;
//            int y1 = y0 + (newY - y0) / 2;
//
//            if (boardcells[x1][y1].hasPiece() && boardcells[x1][y1].getPiece().getType() != piece.getType()) {
//                return new MoveResult(MoveType.KILL, boardcells[x1][y1].getPiece());
//            }
//        }
//
//        return new MoveResult(MoveType.NONE);
//    }

    private int toBoard(double pixel) {
        return (int)(pixel + TILE_SIZE / 2) / TILE_SIZE;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Scene scene = new Scene(initBoard());
        primaryStage.setTitle("Quoridor");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

//    private Piece makePiece(PieceType type, int x, int y) {
//        Piece piece = new Piece(type, x, y);
//
//        piece.setOnMouseReleased(e -> {
//            int newX = toBoard(piece.getLayoutX());
//            int newY = toBoard(piece.getLayoutY());
//
//            MoveResult result;
//
//            if (newX < 0 || newY < 0 || newX >= WIDTH || newY >= HEIGHT) {
//                result = new MoveResult(MoveType.NONE);
//            } else {
//                result = tryMove(piece, newX, newY);
//            }
//
//            int x0 = toBoard(piece.getOldX());
//            int y0 = toBoard(piece.getOldY());
//
//            switch (result.getType()) {
//                case NONE:
//                    piece.abortRelocate();
//                    break;
//                case NORMAL:
//                    piece.relocatePiece(newX, newY);
//                    boardcells[x0][y0].setPiece(null);
//                    boardcells[newX][newY].setPiece(piece);
//                    break;
//                case KILL:
//                    piece.relocatePiece(newX, newY);
//                    boardcells[x0][y0].setPiece(null);
//                    boardcells[newX][newY].setPiece(piece);
//
//                    Piece otherPiece = result.getPiece();
//                    boardcells[toBoard(otherPiece.getOldX())][toBoard(otherPiece.getOldY())].setPiece(null);
//                    pieceGroup.getChildren().remove(otherPiece);
//                    break;
//            }
//        });
//
//        return piece;
//    }

    public static void main(String[] args) {
        launch(args);
    }
}
