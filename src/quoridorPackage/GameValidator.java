/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoridorPackage;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author fm
 */
public class GameValidator {
   
    Board board;
    
    public GameValidator(Board board){
        this.board = board;
    }
    
       /**
     * @param action
     * @return whether the action is valid
     */
    public boolean isValidAction(String action) throws CloneNotSupportedException {
        if (isValidActionString(action)) 
        {
            if(action.length() == 2)
                return isValidMoveAction(new Cell(new Position(action)));
            else if(action.length() == 3){
                
                String orient = String.valueOf(action.charAt(2)).toUpperCase();
                return isValidBlockAction(new Wall(new Position(action), Orientation.valueOf(orient)));
            }
        }
        return false;
    }
    
    public boolean isValidMoveAction(Cell c)
    {
        boolean validity = true;
        List<Position> adjacent = validMoves(board.positionOf(board.pieces[board.getCurrent()]));
        validity = validity && adjacent.contains(c.p);
        return validity;
    }
    public boolean isValidBlockAction(Wall wall) throws CloneNotSupportedException{
        boolean validity = true;
        if (board.getNumOfPiecesWall()[board.getCurrent()] >= 0) {
            Cell northwest = board.getBoardCells()[wall.p.getRow()][wall.p.getCol()];
            Cell northeast = board.getBoardCells()[wall.p.getRow()][wall.p.getCol() + 1];
            Cell southwest = board.getBoardCells()[wall.p.getRow() + 1][wall.p.getCol()];
            //System.out.println(wall.getOrientation());
            if (wall.getOrientation() == Orientation.V) {
                validity = validity
                        && northwest.getNeighbour(Direction.RIGHT) != null;
                validity = validity
                        && southwest.getNeighbour(Direction.RIGHT) != null;
                validity = validity
                        && !(northwest.getNeighbour(Direction.DOWN) == null && northeast
                                .getNeighbour(Direction.DOWN) == null);
                
            } else if (wall.getOrientation() == Orientation.H) {
                validity = validity
                        && northwest.getNeighbour(Direction.DOWN) != null;
                validity = validity
                        && northeast.getNeighbour(Direction.DOWN) != null;
                validity = validity
                        && !(northwest.getNeighbour(Direction.RIGHT) == null && southwest
                                .getNeighbour(Direction.RIGHT) == null);
            } else {
                validity = false;
            }
            
//            if (validity) {
//            // Test if we can flood the board
//            //Board test = board.clone();
//            Board test = Block(wall);
//            if (!flood(test)) {
//                validity = false;
//            }
//        }
            
    }
        return validity;
    }
    
       /**
     * Check if a relocatePiece string is valid.
     * @param move
     * @return 
     */
    public boolean isValidActionString(String move) {
        boolean validity = (move.length() >= 2);
        validity = validity && move.charAt(0) >= 'a' && move.charAt(0) <= 'i';
        validity = validity && move.charAt(1) >= '1' && move.charAt(1) <= '9';
        if (move.length() == 3) {
            validity = validity && (move.charAt(2) == 'V' || move.charAt(2) == 'v'|| move.charAt(2) == 'H' || move.charAt(2) == 'h');
            validity = validity && move.charAt(0) <= 'h';
            validity = validity && move.charAt(1) <= '8';

        } else if (move.length() > 3) {
            return false;
        }
        return validity;
    }
    
    /**
     * Get a list of valid relocatePiece cells from a position
     * @param pm
     * @return reachable empty squares
     */
        
    public List<Position> validMoves(Position pm) {
        LinkedList<Position> adjacent = new LinkedList<>();
        LinkedList<Position> moves = new LinkedList<>();
        LinkedList<Direction> dirs = new LinkedList<>();
        Cell current = board.getBoardCells()[pm.getRow()][pm.getCol()];
        Cell neighbour;
        Direction d;
        for (Direction dir : Direction.values()) {
            neighbour = current.getNeighbour(dir);
            if (neighbour != null) {
                boolean add = adjacent.add(neighbour.getPosition());
                dirs.add(dir);
            }
        }
        for (Position p : adjacent) {
            d = dirs.removeFirst();
//            if (playerAt(p) != null) {
//                for (Position pos: jump(p, d, false)) {
//                    moves.add(pos);
//                }
//            } else 
            {
                moves.add(p);
            }
        }
        return moves;
    }
}
