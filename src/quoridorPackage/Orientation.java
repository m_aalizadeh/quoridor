/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoridorPackage;

 public enum Orientation {
	H(0), V(1);
    private final int index;   

    Orientation(int index) {
        this.index = index;
    }

    private int index() { 
        return index; 
    }
    }