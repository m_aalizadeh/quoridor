/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoridorPackage;

/**
 *
 * @author fm
 */

public abstract class Action {

    protected String actionStr;
    BoardItem boardItem;
    Position p;
    
    public Action (String actionStr){
        this.actionStr = actionStr;
    }
    
    public Action(String actionStr, BoardItem boardItem){
        this.actionStr = actionStr;
        this.boardItem = boardItem;
    }
    
}
