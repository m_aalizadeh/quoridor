package quoridorPackage;


import java.util.Arrays;
import javafx.scene.paint.Color;
import java.util.List;
import java.util.LinkedList;
import org.jgrapht.Graph;

/*
 * Represents the game board.
 */
public class Board {
    
    final int dim = 9;
    int walls = 20;
    private int current = 0;
    Piece[] pieces;
    Wall[] wallss;
    Cell[][] boardcells;
    int[] numOfPiecesWall;
    BoardGraph boardGraph;
    String[] targetCells_1 = {"a1","b1", "c1", "d1","e1","f1","g1","h1","i1"};
    String[] targetCells_2 = {"a9","b9", "c9", "d9","e9","f9","g9","h9","i9"};
   
    private void makeBoardcells() {
                
        boardcells = new Cell[dim][dim];
        for (int i = 0; i < boardcells.length; i++) {
            for (int j = 0; j < boardcells.length; j++) {
                boardcells[i][j] = new Cell(new Position(i, j));
            }
        }
        for (int i = 1; i < boardcells.length; i++) {
            for (int j = 0; j < boardcells.length; j++) {
                boardcells[i][j].setNeighbour(Direction.UP, boardcells[i - 1][j]);
            }
        }
        for (int i = 0; i < boardcells.length - 1; i++) {
            for (int j = 0; j < boardcells.length; j++) {
                boardcells[i][j].setNeighbour(Direction.DOWN, boardcells[i + 1][j]);
            }
        }
        for (int i = 0; i < boardcells.length; i++) {
            for (int j = 1; j < boardcells.length; j++) {
                boardcells[i][j].setNeighbour(Direction.LEFT, boardcells[i][j - 1]);
            }
        }
        for (int i = 0; i < boardcells.length; i++) {
            for (int j = 0; j < boardcells.length - 1; j++) {
                boardcells[i][j].setNeighbour(Direction.RIGHT, boardcells[i][j + 1]);
            }
        }
        
        
    }
    
    
    /**
     * Construct a new board object initialised with a set of players
     * @param players
     */
    public Board(Player[] players) {
        
        //init board's cells
        makeBoardcells();
        
        //init board's graph
        boardGraph = new BoardGraph(boardcells);
        boardGraph.showGraph();
        
        // init pieces
        this.pieces = new Piece[players.length];
        Direction[] pieces_default_dir = { Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT };
        Color[] pieces_color = {Color.BLUE, Color.RED, Color.GREEN, Color.YELLOW};
        Position[] pieces_default_pos = { new Position(8, 4), new Position(0, 4), new Position(4, 8), new Position(4, 0) };
        if (pieces.length != 2 && pieces.length != 4) { throw new IllegalArgumentException(
                "Only 2 or 4 players are supported."); }
        for (int i = 0; i < pieces.length; i++) {
            Position m = pieces_default_pos[i];
            pieces[i] = new Piece(m, i+1, pieces_color[i], pieces_default_dir[i]);
            boardcells[m.getRow()][m.getCol()].setPiece(pieces[i]);
        }
        //initialize number of each piece's wall:
        this.numOfPiecesWall = new int [pieces.length];
        for(int i=0; i<numOfPiecesWall.length; i++) 
            this.numOfPiecesWall[i] = walls/pieces.length;
    }
    

     private Board(Piece[] pieces, Cell[][] boardcells, int current, int[] numOfPiecesWall, BoardGraph boardGraph) {
        this.pieces = pieces;
        this.current = current;
        this.boardcells = boardcells;
        this.numOfPiecesWall = numOfPiecesWall;
        this.boardGraph = boardGraph;
    }
    
    /**
     * Print the current state of the board.
     */
    public void printBoard() {
        for (Piece current : pieces) {
            System.out.println("Player " + current.getPieceNum() + " ("
                    + current.getColor() + ") has "
                    + getRemainingWalls(current.getPieceNum()) + " walls remaining.");
        }
        System.out.println(" [4m a b c d e f g h i [24m");
        for (int i = 0; i < boardcells.length; i++) {
            System.out.print(i + 1);
            System.out.print("|");
            for (Cell cell : boardcells[i]) {
                String cellname;
                if (cell.getContainedPiece() == null) {
                    cellname = " ";
                } else {
                    cellname = String.valueOf(cell.getContainedPiece().getPieceNum());
                    if (cell.getContainedPiece().getPieceNum() == getCurrentPieceNum()) {
                        cellname = "[1m" + cellname + "[0m";
                    }
                }
                if (cell.getNeighbour(Direction.DOWN) != null && i != 8) {
                    
                    cellname = "[24m" + cellname;
                } else {
                    
                    cellname = "[4m" + cellname;
                    if (cell.getContainedPiece() != null
                            && cell.getContainedPiece().getPieceNum() == getCurrentPieceNum()) {
                        cellname += "[4m";
                    }
                }
                if (cell.getNeighbour(Direction.RIGHT) == null) {
                    cellname += "|";
                } else if (i != 8) {
                    cellname += ".";
                } else {
                    cellname += " ";
                }
                System.out.print(cellname);
            }
            System.out.println("[24m");
        }
        System.out.println("It is " + pieces[current].getColor() + "'s turn.");
    }
    
    /**
     * Get a list of the numbers of all players in the game.
     * @return 
     */
    public List<Integer> getPieces() {
        List<Integer> ps = new LinkedList<>();
        for (Piece p : pieces) {
            ps.add(p.getPieceNum());
        }
        return ps;
    }
    
    /**
     * Get the number of walls a given player has remaining.
     * @param pieceNum
     * @return 
     */
    public int getRemainingWalls(int pieceNum) {
        return numOfPiecesWall[pieceNum-1];
    }
    
    public void decRemainingWalls(int pieceNum) {
         this.numOfPiecesWall[pieceNum-1]--;
    }
    
    
    /**
     * Get the position of a player
     * @param p
     * @return 
     */
    public Position positionOf(Piece p) {
        for (int i = 0; i < boardcells.length; i++) {
            for (int j = 0; j < boardcells.length; j++) {
                if (p.equals(boardcells[i][j].getContainedPiece())) {
                    return new Position(i,j);
                }
            }
        }
        return null;
    }
    
    
    /**
     * Get the position of the player with a specified number
     * @param id
     * @return 
     */
    public Position positionOf(int pieceNum) {
        return positionOf(pieces[pieceNum - 1]);
    }
    
    /**
     * Get the id of the player at the specified position
     * @param p
     * @return 
     */
    public Integer playerAt(Position p) {
        Piece pl = boardcells[p.getRow()][p.getCol()].getContainedPiece();
        if (pl != null) {
            return pl.getPieceNum();
        } else {
            return null;
        }
    }

    public Cell[][] getBoardCells(){
        return boardcells;
    }
    
    public int getCurrent(){
        return current;
    }
    
    /**
     * Get the current player.
     * returns whose turn it is
     * @return 
     */
    public int getCurrentPieceNum() {
        return pieces[current].getPieceNum();
    }
    
    
    public void setCurrentPiece(int current){
        this.current = current;
    }
    
    /**
     * Move the current piece to the specified position.
     */
    void MovePiece(Piece piece) {
        Piece p = pieces[current];
        Position from = positionOf(p);
        boardcells[from.getRow()][from.getCol()].setPiece(null);
        boardcells[piece.p.getRow()][piece.p.getCol()].setPiece(p);
    }
    
    /**
     * Place a wall at the given position.
     */
        
    void BlockWithWall(Wall wall) throws CloneNotSupportedException {
        Position position = wall.p;
        Orientation orientation = wall.getOrientation();
        Cell northwest = boardcells[position.getRow()][position.getCol()];
        Cell northeast = boardcells[position.getRow()][position.getCol() + 1];
        Cell southwest = boardcells[position.getRow() + 1][position.getCol()];
        Cell southeast = boardcells[position.getRow() + 1][position.getCol() + 1];
        
        
        BoardGraph retval = boardGraph.clone();
        
        if (orientation == Orientation.V) {
            northwest.setNeighbour(Direction.RIGHT, null);
            northeast.setNeighbour(Direction.LEFT, null);
            southwest.setNeighbour(Direction.RIGHT, null);
            southeast.setNeighbour(Direction.LEFT, null);
            
            retval.removeEdges(northwest.p.toString(), northeast.p.toString());
            retval.removeEdges(southwest.p.toString(), southeast.p.toString());
            
            
        } else if(orientation == Orientation.H){
            northwest.setNeighbour(Direction.DOWN, null);
            northeast.setNeighbour(Direction.DOWN, null);
            southwest.setNeighbour(Direction.UP, null);
            southeast.setNeighbour(Direction.UP, null);
            
            //Update_BoardGraph(boardGraph);
            retval.removeEdges(northwest.p.toString(), southwest.p.toString());
            retval.removeEdges(northeast.p.toString(), southeast.p.toString());
        }
        
        boardGraph = retval;
        boardGraph.showGraph();

    }
    
    @Override
    public Board clone() throws CloneNotSupportedException {
        return new Board(pieces, boardcells, current, numOfPiecesWall, boardGraph);
    }

    public int[] getNumOfPiecesWall() {
        return this.numOfPiecesWall;
    }

    private void setNumOfPiecesWall(int[] numOfPiecesWall) {
        this.numOfPiecesWall = numOfPiecesWall;
    }
    
}
