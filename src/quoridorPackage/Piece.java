/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoridorPackage;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import static quoridorPackage.MainUI.TILE_SIZE;

/**
 *
 * @author fm
 */
public class Piece extends StackPane{

    private Color color;
    private int pieceNum;
    private Direction dir;
    ///////////////////
    Position p;
    private PieceColor type;
    private double mouseX, mouseY;
    

    public Piece(Position p, int pieceNum, Color c, Direction dir) {
        this.p = p;
        this.dir = dir;
        this.color = c;
        this.pieceNum = pieceNum;
    }
    
    public Piece(Position p) {
        this.p = p;
    }

    /**
     * Get the player's ID.
     * @return 
     */
    public final int getPieceNum() {
        return pieceNum;
    }

    Color getColor() {
        return color;
    }
    
    /**
     * Get the direction of the target wall. when a player won the game
     * @return 
     */
    public final Direction getEnd() {
        return this.dir;
    }
    
    /////////////////////////////////////////////
     public Piece(PieceColor type, Position p) {
        this.type = type;
        relocatePiece(p.getRow(), p.getCol());

        Ellipse ellipse = new Ellipse(TILE_SIZE * 0.3125, TILE_SIZE * 0.26);
        ellipse.setFill(Color.RED);

        ellipse.setStroke(Color.BLACK);
        ellipse.setStrokeWidth(TILE_SIZE * 0.03);

        ellipse.setTranslateX((TILE_SIZE - TILE_SIZE * 0.3125 * 2) / 2);
        ellipse.setTranslateY((TILE_SIZE - TILE_SIZE * 0.26 * 2) / 2);

        getChildren().addAll(ellipse);

        setOnMousePressed(e -> {
            mouseX = e.getSceneX();
            mouseY = e.getSceneY();
        });

        setOnMouseDragged(e -> {
            relocate(e.getSceneX() - mouseX + p.getRow(), e.getSceneY() - mouseY + p.getCol());
        });
    }
     
      public void relocatePiece(int x, int y) {
//        oldX = x * TILE_SIZE;
//        oldY = y * TILE_SIZE;
        p.setRow(x * TILE_SIZE);
        p.setCol(y * TILE_SIZE);
        relocate(p.getRow(), p.getCol());
    }

    public void abortRelocate() {
        relocate(p.getRow(), p.getCol());
    }

}
