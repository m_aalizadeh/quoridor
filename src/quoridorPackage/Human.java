package quoridorPackage;


import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Human extends Player {
    
    public Human() {
        super();
    }
    
    @Override
    public void onFailure(Board b) {
        System.out.println("Invalid move!");
    }
    
    @Override
    public String nextAction(Game g) {
        
        String temp = "";
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try {
            g.getBoard().printBoard();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Human.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.print("Enter the move for player "
                + (this.getName()) + ": ");
        try {
            temp = in.readLine();
        } catch (IOException e) {
        }
        return temp;
    }
    
}
