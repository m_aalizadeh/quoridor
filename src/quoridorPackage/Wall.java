/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoridorPackage;

/**
 *
 * @author fm
 */
public class Wall extends BoardItem{
    
    private Orientation orientation;
    Cell[] adjacent;
    
    public Wall(Position p, Orientation o) {
        super(p);
        this.orientation = o;
    }
    
    public Wall(Position p) {
        super(p);
    }
    
    public Orientation getOrientation(){
        return this.orientation;
    }
    
}
