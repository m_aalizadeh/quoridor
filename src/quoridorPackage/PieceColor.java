/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoridorPackage;

/**
 *
 * @author fm
 */

public enum PieceColor {
    RED(1), WHITE(-1);

    final int moveDir;

    PieceColor(int moveDir) {
        this.moveDir = moveDir;
    }
}