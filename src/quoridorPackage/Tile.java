/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoridorPackage;

/**
 *
 * @author fm
 */
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;


public class Tile extends Rectangle {

    private Piece piece;
    Tile[] adjacent;
    Position p;

    public boolean hasPiece() {
        return piece != null;
    }

//    public Piece getPiece() {
//        return piece;
//    }
//
//    public void setPiece(Piece piece) {
//        this.piece = piece;
//    }
    
//    public Tile(boolean light, int x, int y) {
//        setWidth(MainUI.TILE_SIZE);
//        setHeight(MainUI.TILE_SIZE);
//
//        relocate(x * MainUI.TILE_SIZE, y * MainUI.TILE_SIZE);
//
//        setFill(light ? Color.valueOf("#feb") : Color.valueOf("#582"));
//    }
    
    /////////////////////////////////////////
    public Tile(Position p) {
        this.p = p;
        this.adjacent = new Tile[4];
        
        setWidth(MainUI.TILE_SIZE);
        setHeight(MainUI.TILE_SIZE);
        relocate(p.getRow() * MainUI.TILE_SIZE, p.getCol() * MainUI.TILE_SIZE);

        setFill(Color.valueOf("#feb"));
    }
    
    public void setNeighbour(Direction direction, Tile t) {
        adjacent[direction.index()] = t;
    }
    
    Piece getContainedPiece() {
        return piece;
    }
    
    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    Tile getNeighbour(Direction direction) {
        return adjacent[direction.index()];
    }
    
    /**
     * Get the position of the cell.
     */
    Position getPosition() {
        return this.p;
    }
    
}
