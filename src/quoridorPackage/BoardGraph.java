/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoridorPackage;


import java.util.List;
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;


/**
 * A simple introduction to using JGraphT.
 *
 * @author Barak Naveh
 */
public final class BoardGraph
{
    Graph<String, DefaultEdge> boardGraph;// = new SimpleGraph<>(DefaultEdge.class);

    public BoardGraph(Cell[][] boardcells){
        this.boardGraph = createGraph(boardcells);
    }
    
    public BoardGraph(Graph boardGraph){
        this.boardGraph = boardGraph;
    }

    public void showGraph()
    {

        System.out.println("Graph State is like this");
        System.out.println(boardGraph.toString());
        System.out.println();
        
        //findShortestPath("v1", "v5", stringGraph);
        
    }

    public static Graph<String, DefaultEdge> createStringGraph()
    {
        Graph<String, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);

        String v1 = "v1";
        String v2 = "v2";
        String v3 = "v3";
        String v4 = "v4";
        String v5 = "v5";

        // add the vertices
        g.addVertex(v1);
        g.addVertex(v2);
        g.addVertex(v3);
        g.addVertex(v4);
        g.addVertex(v5);

        // add edges to create a circuit
        g.addEdge(v1, v2);
        g.addEdge(v2, v3);
        g.addEdge(v3, v4);
        g.addEdge(v4, v1);
        g.addEdge(v2, v5);
        g.addEdge(v1, v5);

        return g;
    }

    public static GraphPath findShortestPath(String src, String dst, Graph graph) {
    
        // Find shortest paths
	GraphPath sp = DijkstraShortestPath.findPathBetween(graph, src, dst);
	
	// Print results
//	System.out.println("Shortest path from " + src + " to " + dst + ":");
//	for (Object e : sp.getEdgeList()) {
//		System.out.println(graph.getEdgeSource(e) + " -> " + graph.getEdgeTarget(e));
////                System.out.println(graph.getEdgeTarget(e) + " -> " + graph.getEdgeSource(e));
//	}
        
        return sp;
//        List node = sp.getVertexList();
//        return node.get(1).toString();
    }

    
    
    private Graph createGraph(Cell[][] boardcells) {
        
        Graph<String, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
        
        //add vertices
        for (int i = 0; i < boardcells.length; i++) {
            for (int j = 0; j < boardcells.length; j++){
              g.addVertex(boardcells[i][j].getPosition().toString());  
            }
        }
        
        //add edges
        for (int i = 0; i < boardcells.length; i++) {
            for (int j = 0; j < boardcells.length; j++){
                for (int k=0; k<4; k++){
                    if (boardcells[i][j].adjacent[k] != null)
                        g.addEdge(boardcells[i][j].getPosition().toString(), boardcells[i][j].adjacent[k].getPosition().toString());
                }                    
            }
        }
        
        return g;
    }


    
    public void removeEdges(String v1, String v2){
        boardGraph.removeEdge(v1, v2);
    }
    
    
    public Graph getGraph(){
        return this.boardGraph;
    }
    
    @Override
    public BoardGraph clone() throws CloneNotSupportedException {
        return new BoardGraph(boardGraph);
    }
    
    
}



