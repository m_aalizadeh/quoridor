/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoridorPackage;

/**
 *
 * @author fm
 */
import java.util.ArrayList;
import java.util.Collections;
import static java.util.Collections.list;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgrapht.GraphPath;

public class AI extends Player {
    
    public AI(){
        super();
    }
	
    public String getRandomAction(Game g) throws CloneNotSupportedException {

        Random rand = new Random();
        int actType = rand.nextInt(2);
        
        // Move
        if(actType==0){
            //System.out.println("current piece: "+ g.getBoard().getCurrentPieceNum());
            List<Position> validActions= g.validMoves(g.getBoard().positionOf(g.getBoard().getCurrentPieceNum()));
            int rand_index = rand.nextInt(validActions.size());
            Position newPos = validActions.get(rand_index);
            System.out.println("here is AI's move: "+ newPos.toString());
            return newPos.toString();
        }
        //Block
        else{
            int row = rand.nextInt(8) + 1;        
        
            final String alphabet = "abcdefgh";
            final int N = alphabet.length();
            char col = alphabet.charAt(rand.nextInt(N));

            final String orientation = "hv";
            final int O = orientation.length();
            char orient = orientation.charAt(rand.nextInt(O));

            System.out.println("here is AI's move: "+String.valueOf(col)+String.valueOf(row)+String.valueOf(orient));
        
            return String.valueOf(col)+String.valueOf(row)+String.valueOf(orient);
        }        
}

    
    
    public String getShortestPath(Game g){
        
        //find all shortest path to oposite side of the board:
        List<GraphPath> paths = new ArrayList<>();
        List<Integer> pathsLength = new ArrayList<>();
        for (String dst : g.board.targetCells_2){
            GraphPath gp = BoardGraph.findShortestPath(g.board.positionOf(g.board.getCurrentPieceNum()).toString(),dst, g.board.boardGraph.boardGraph);
            paths.add(gp);
            pathsLength.add(gp.getLength());
        }
        int minIndex = pathsLength.indexOf(Collections.min(pathsLength));
        List nextPath = paths.get(minIndex).getVertexList();
        String nextAct = nextPath.get(1).toString();
        System.out.println("AI's next action is: "+ nextAct);
        return nextAct;
    }
    
    
    @Override
    public String nextAction(Game g) {
        Board b = null;
        try {
            b = g.getBoard();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(AI.class.getName()).log(Level.SEVERE, null, ex);
        }
        String retval = "";
//        retval = getShortestPath(g);
        retval = getShortestPath(g);
        b.printBoard();
        return retval;
    }
    
      @Override
    public void onFailure(Board b) {
        System.out.println("Invalid AI move!");
    }
	
	
}
