//package aston.group12.view;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import aston.group12.controller.StatsController;
import aston.group12.model.Board;
import aston.group12.model.GameSession;
import aston.group12.model.Player;
import aston.group12.model.RuleType;
import aston.group12.model.Settings;
import aston.group12.model.Statistics;
import aston.group12.model.Tile;
import aston.group12.view.components.HorizontalWallComponent;
import aston.group12.view.components.PawnComponent;
import aston.group12.view.components.PawnComponent.PawnType;
import aston.group12.view.components.TileComponent;
import aston.group12.view.components.VerticalWallComponent;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class MainGame extends Application implements GameScreen {

	public static final int TILE_SIZE = Settings.getSingleton().getTileSize();


	private List<PawnComponent> pawnComponentList = new ArrayList<PawnComponent>(GameSession.MAX_PLAYERS);
	private GameSession gameSession;
	private int height;
	private int width;
	private PawnType[] pawnTypes = {PawnType.RED, PawnType.WHITE, PawnType.GREEN, PawnType.BLUE}; //Could possibly loop through the pawnTypes enums instead of storing in an array
	private int turnIndex;

	private CellComponent[][] tileBoard;
	private HorizontalWallComponent[][] horizontalWalls;
	private VerticalWallComponent[][] verticalWalls;
	private Group CellGroup = new Group();
	private Group pawnGroup = new Group();
	private Group horizontalWallGroup = new Group();
	private Group verticalWallGroup = new Group();
	private Label currentTurnLabel;
	private Label wallsLabel;
	private Scene scene;



	private Parent createContent(){

		Pane root = new Pane();
		root.setPrefSize((width * TILE_SIZE) + 85, height * TILE_SIZE);
		root.getChildren().addAll(CellGroup, pawnGroup, horizontalWallGroup, verticalWallGroup, infoPanel());


		// Add Tile
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				CellComponent tile = new CellComponent(x, y);
				CellBoard[x][y] = tile;
				CellGroup.getChildren().add(tile);
			}
		}

		//Add Wall
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

			}

		}
	}

    
}
