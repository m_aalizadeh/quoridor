/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quoridorPackage;

/**
 *
 * @author fm
 */
public class Cell extends BoardItem{

    Piece piece;
    Cell[] adjacent;
    Integer cellNum;
            
    public Cell(Position p) {
        super(p);
        this.adjacent = new Cell[4];
    }
    
    public void setNeighbour(Direction direction, Cell c) {
        adjacent[direction.index()] = c;
    }

    void setPiece(Piece piece) {
        this.piece = piece;
    }

    Piece getContainedPiece() {
        return piece;
    }

    Cell getNeighbour(Direction direction) {
        return adjacent[direction.index()];
    }
    
    /**
     * Get the position of the cell.
     */
    Position getPosition() {
        return this.p;
    }
    
}
