package quoridorPackage;


public abstract class Player {

    private Direction direction;
    protected String  name;
    
    
    public Player(String name) {
        this.name = name;
    }
    
    public Player() {
    }
    
    
    /**
     * Get the direction of the target wall. when a player won the game
     * @return 
     */
    public final Direction getEnd() {
        return this.direction;
    }
    
       
    /**
     * get the player's next move given a certain game state.
     * @param g
     * @return 
     * @throws java.lang.CloneNotSupportedException 
     */
    public final String getAction(Game g) throws CloneNotSupportedException {
        String move = this.nextAction(g);
        while (!g.isValidAction(move)) {
            onFailure(g.getBoard());
            move = this.nextAction(g);
        }
        
        return move;
    }
    
    /**
     * Get the name of the player.
     * @return name of the player
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Set the player's name.
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Generate the player's next move.
     * @param b
     * @return 
     */
    public abstract String nextAction(Game g);

    public void onFailure(Board board) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

