package quoridorPackage;

import java.io.*;
import java.util.Arrays;

public class Main {
	private static Game game;
        private static Tournoment tournoment;
	
        /*gets players and starts the game*/
	public static void main (String[] argv) throws IOException, CloneNotSupportedException{ 
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Welcome to Quoridor Game!");
		String userInput = "";
		Integer playersCount =0;
		Player[] players;
		while(playersCount != 2 && playersCount != 4){
			userInput = "";
			while(!ifIsInteger(userInput)){
				System.out.print("Number of players (2/4): ");
				userInput = in.readLine();
			}

			playersCount = Integer.parseInt(userInput);
		}
		players = new Player[playersCount];
		
		for (Integer i = 1; i<= playersCount; i++){
			userInput = "";
			while (!userInput.toLowerCase().matches("(ai?|h(uman)?)")) {
				System.out.print("Player " + i.toString() + " is a human or AI? ");
				userInput = in.readLine();
			}
			if (userInput.toLowerCase().matches("h(uman)?")){
				players[i - 1] = new Human();

                        System.out.print("Player " + i.toString() + " name: ");
                        userInput = in.readLine();
                        players[i - 1].setName(userInput);
			}
                        
                        
                        else {
				
				players[i - 1] = new AI();
				players[i - 1].setName("nana");
				
			}
                        
                        
		}
//		game = new Game(players);
//		game.play();
//		game.getBoard().printBoard();
//              System.out.println("Player "+ game.getWinner().getColor() + " won the game!");
                tournoment = new Tournoment(players);
                tournoment.PlayAllGames();
	}
	/**
	 * @param string which is meant to be an int
	 * @return false or true if correct parse
	 */
	static boolean ifIsInteger(String value)  
	{  
	     try  
	     {  
	         Integer.parseInt(value);  
	         return true;  
	      } catch(NumberFormatException nfe)  
	      {  
	          return false;  
	      }  
	}
}
