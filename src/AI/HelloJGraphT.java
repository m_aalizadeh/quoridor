/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AI;


import com.sun.javafx.geom.Edge;
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.io.*;
import org.jgrapht.traverse.*;

import java.io.*;
import java.net.*;
import java.util.*;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;


/**
 * A simple introduction to using JGraphT.
 *
 * @author Barak Naveh
 */
public final class HelloJGraphT
{

    
    private HelloJGraphT()
    {
    } // ensure non-instantiability.

    /**
     * The starting point for the demo.
     *
     * @param args ignored.
     *
     * @throws URISyntaxException if invalid URI is constructed.
     * @throws ExportException if graph cannot be exported.
     */
    public static void main(String[] args)
        throws URISyntaxException,
        ExportException
    {
        Graph<String, DefaultEdge> stringGraph = createStringGraph();

        // note undirected edges are printed as: {<v1>,<v2>}
        System.out.println("-- toString output");
        System.out.println(stringGraph.toString());
        System.out.println();
        
        
        findShortestPath("v1", "v5", stringGraph);
        

    }

    private static Graph<String, DefaultEdge> createStringGraph()
    {
        Graph<String, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);

        String v1 = "v1";
        String v2 = "v2";
        String v3 = "v3";
        String v4 = "v4";
        String v5 = "v5";

        // add the vertices
        g.addVertex(v1);
        g.addVertex(v2);
        g.addVertex(v3);
        g.addVertex(v4);
        g.addVertex(v5);

        // add edges to create a circuit
        g.addEdge(v1, v2);
        g.addEdge(v2, v3);
        g.addEdge(v3, v4);
        g.addEdge(v4, v1);
        g.addEdge(v2, v5);
        g.addEdge(v1, v5);

        return g;
    }



    private static void findShortestPath(String src, String dst, Graph graph) {
    
        // Find shortest paths
	GraphPath sp = DijkstraShortestPath.findPathBetween(graph, src, dst);
	
	// Print results
	System.out.println("Shortest path from " + src + " to " + dst + ":");
	for (Object e : sp.getEdgeList()) {
		System.out.println(graph.getEdgeSource(e) + " -> " + graph.getEdgeTarget(e));
	}
    
    }
    
}
